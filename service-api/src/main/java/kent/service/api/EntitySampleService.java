/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kent.service.api;

import java.util.List;
import javax.ws.rs.core.Response;

/**
 *
 * @author kent_chen
 */
public interface EntitySampleService {
    
    /**
     * 取得全部EntitySample
     * @return 含所有EntitySample的List
     */
    List<EntitySample> getAll();
    
    /**
     * 取得id對應之單一EntitySample
     * @param id
     * @return 單一EntitySample
     */
    EntitySample get(Long id);
    
    /**
     * 增加一個EntitySample的Instance
     * @param es
     * @return 201 CREATED的Response，Body被新增的EntitySample
     */
    Response add(EntitySample es);
    
    /**
     * 更新id對應之單一EntitySample
     * @param id
     * @param es
     * @return 被更新的EntitySample
     */
    EntitySample update(Long id, EntitySample es);
    
    /**
     * 刪除id對應之單一EntitySample
     * @param id
     */
    void delete(Long id);
    
}
