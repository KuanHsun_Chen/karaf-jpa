/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kent.service.api;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * 
 * @author kent_chen
 * 
 * 僅包含val1這一個column的Entity，作為簡易範例使用。
 */
@Entity
public class EntitySample implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    private String val1;
    
    /**
     * 取得id
     * @return id 
     */
    public Long getId() {
        return id;
    }
    
    /**
     * 設定id
     * @param id 
     */
    public void setId(Long id) {
        this.id = id;
    }
    
    /**
     * 取得val1
     * @return val1
     */
    public String getVal1() {
        return val1;
    }
    
    /**
     * 設定val1
     * @param val1 
     */
    public void setVal1(String val1) {
        this.val1 = val1;
    }

}
